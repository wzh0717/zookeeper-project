﻿using org.apache.utils;
using org.apache.zookeeper;
using org.apache.zookeeper.data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zookeeper.Client
{
    /// <summary>
    /// Zookeeper 操作类
    /// created by wangzh 2020/06/12
    /// </summary>
    public class ZookeeperHelper : Watcher, IDisposable
    {
        //CreateMode类
        //PERSISTENT：持久化节点
        //PERSISTENT_SEQUENTIAL：持久化有序节点
        //EPHEMERAL：临时节点（连接断开自动删除）
        //EPHEMERAL_SEQUENTIAL：临时有序节点（连接断开自动删除）

        //ZooKeeper.States的枚举
        //CONNECTING = 0,　　//连接中
        //CONNECTED = 1,　　 //已连接
        //CONNECTEDREADONLY = 2,　　//已连接，但是只能只读访问
        //CLOSED = 3,　　　　//已关闭连接
        //AUTH_FAILED = 4,　　　　//认证失败
        //NOT_CONNECTED = 5　　//未连接

        //监听事件响应状态，Watcher.Event.KeeperState的枚举
        //Expired = -112,　　　　//连接超时
        //Disconnected = 0,　　　　//连接断开
        //SyncConnected = 3,　　　　//已同步连接
        //AuthFailed = 4,　　　　//认证失败
        //ConnectedReadOnly = 5　　　　//只读连接　

        //监听事件类型，Watcher.Event.EventType的枚举　　
        //None = -1,　　　　//非节点操作事件
        //NodeCreated = 1,　　　　//创建节点事件
        //NodeDeleted = 2,　　　　//删除节点事件
        //NodeDataChanged = 3,　　　　//节点数据改变
        //NodeChildrenChanged = 4　　　　//子节点发生改变

        /// <summary>
        /// Zookeeper路径分隔符
        /// </summary>
        String sep = "/";

        /// <summary>
        /// Zookeeper访问对象
        /// </summary>
        ZooKeeper zookeeper;

        /// <summary>
        /// Zookeeper连接字符串，采用host:port格式，多个地址之间使用英文逗号（,）隔开
        /// </summary>
        String connectionString;

        /// <summary>
        /// 路径监控节点列表
        /// </summary>
        ConcurrentDictionary<String, NodeWatcher> nodeWatchers = new ConcurrentDictionary<String, NodeWatcher>();

        /// <summary>
        /// 节点的默认权限
        /// </summary>
        List<ACL> defaultACL = ZooDefs.Ids.OPEN_ACL_UNSAFE;

        /// <summary>
        /// 默认的监听器
        /// </summary>
        DefaultWatcher defaultWatcher;

        /// <summary>
        /// 监控定时器
        /// </summary>
        System.Timers.Timer timer;

        /// <summary>
        /// 同步锁
        /// </summary>
        AutoResetEvent are = new AutoResetEvent(false);

        /// <summary>
        /// 是否正常关闭
        /// </summary>
        Boolean isClose = false;


        /// <summary>
        /// 回话超时时间
        /// </summary>
        public Int32 SessionTimeout { get; set; } = 10000;

        /// <summary>
        /// 当前路径
        /// </summary>
        public String CurrentPath { get; private set; }

        /// <summary>
        /// 是否已连接Zookeeper
        /// </summary>
        public Boolean Connected { get { return zookeeper != null && (zookeeper.getState() == ZooKeeper.States.CONNECTED || zookeeper.getState() == ZooKeeper.States.CONNECTEDREADONLY); } }

        /// <summary>
        /// Zookeeper是否有写的权限
        /// </summary>
        public Boolean CanWrite { get { return zookeeper != null && zookeeper.getState() == ZooKeeper.States.CONNECTED; } }

        /// <summary>
        /// 数据编码
        /// </summary>
        public Encoding Encoding { get; set; } = Encoding.UTF8;

        /// <summary>
        /// 释放时发生
        /// </summary>
        public event Action OnDisposing;

        /// <summary>
        /// 在重新连接时发生
        /// </summary>
        public event Action OnConnected;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">集群地址(host:prot)</param>
        public ZookeeperHelper(String connectionString) : this(connectionString, "")
        {
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">集群地址(host:prot)</param>
        /// <param name="root">初始化根路经</param>
        public ZookeeperHelper(String connectionString, String root)
        {
            this.connectionString = connectionString;
            CurrentPath = String.IsNullOrWhiteSpace(root) ? sep : root;

            SetLogger(new ZookeeperLogger());

            timer = new System.Timers.Timer
            {
                Interval = 5000
            };
            timer.Elapsed += Timer_Elapsed;
        }

        public ZooKeeper GetZooKeeper()
        {
            return new ZooKeeper(connectionString, SessionTimeout, new InitWatcher("Connection"), false);
        }

        /// <summary>
        /// Zookeeper的日志设置
        /// </summary>
        /// <param name="log"></param>
        public static void SetLogger(ZookeeperLogger log)
        {
            ZooKeeper.LogLevel = log.LogLevel;
            ZooKeeper.LogToFile = log.LogToFile;
            ZooKeeper.LogToTrace = log.LogToTrace;
            ZooKeeper.CustomLogConsumer = log;
        }

        #region 私有方法

        /// <summary>
        /// 定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;

            if (Monitor.TryEnter(timer))//每次只能一个线程进去
            {
                if (!isClose)
                {
                    if (!Connected)
                    {
                        try
                        {
                            zookeeper?.closeAsync();
                            are.Reset();
                            zookeeper = new ZooKeeper(connectionString, SessionTimeout, defaultWatcher);
                            if (are.WaitOne(SessionTimeout) && Connected)//会话未超时，表示成功连接
                            {
                                //挂载监听器
                                foreach (var key in nodeWatchers.Keys)
                                {
                                    NodeWatcher watcher;
                                    if (nodeWatchers.TryGetValue(key, out watcher))
                                    {
                                        WatchAsync(key, watcher, true).Wait();
                                    }
                                }
                                OnConnected?.Invoke();
                                Monitor.Exit(timer);
                                return;
                            }
                        }
                        catch { }
                        timer.Enabled = true;
                    }
                }

                Monitor.Exit(timer);
            }
        }

        /// <summary>
        /// 检查连接是否正常
        /// </summary>
        private void CheckConnection()
        {
            if (!Connected)
            {
                throw new Exception("fail to connect to the server:" + connectionString);
            }
        }

        /// <summary>
        /// 检查是否具有写的权限
        /// </summary>
        private void CheckWriten()
        {
            if (!CanWrite)
            {
                throw new Exception("this connection is in readonly mode");
            }
        }

        /// <summary>
        /// 连接数据成Zookeeper的路径格式
        /// </summary>
        /// <param name="paths">路径</param>
        /// <returns>连接后的路径</returns>
        private String Combine(params String[] paths)
        {
            List<String> list = new List<String>();
            foreach (var path in paths)
            {
                var ps = path.Split(new String[] { "/", "\\" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var p in ps)
                {
                    if (p.Equals("."))//当前路径
                    {
                        continue;
                    }
                    else if (p.Equals(".."))//回退
                    {
                        if (0 == list.Count)
                        {
                            throw new ArgumentOutOfRangeException("path is out of range");
                        }

                        list.RemoveAt(list.Count - 1);
                    }
                    else
                    {
                        list.Add(p);
                    }
                }
            }

            return sep + String.Join(sep, list.ToArray());
        }

        /// <summary>
        /// 使用指定的分隔符连接路径
        /// </summary>
        /// <param name="sep">分隔符</param>
        /// <param name="paths">路径</param>
        /// <returns>连接后的路径</returns>
        private String MakePathName(String sep, params String[] paths)
        {
            List<String> list = new List<String>();
            foreach (var path in paths)
            {
                var ps = path.Split(new String[] { "/", "\\" }, StringSplitOptions.RemoveEmptyEntries);
                list.AddRange(ps);
            }

            return String.Join(sep, list.ToArray());
        }
        /// <summary>
        /// 获取绝对路径
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="isAbsolute">路径是否是绝对路径</param>
        /// <returns>绝对路径</returns>
        private String GetAbsolutePath(String path, Boolean isAbsolute)
        {
            if (!isAbsolute)
            {
                path = Combine(CurrentPath, path);
            }
            else
            {
                path = Combine(path);
            }
            return path;
        }

        #endregion

        /// <summary>
        /// 连接Zookeeper
        /// </summary>
        /// <returns>成功连接返回true,否则返回false</returns>
        public Boolean Connect()
        {
            if (Connected)
            {
                return true;
            }
            if (null == zookeeper)
            {
                lock (this)
                {
                    defaultWatcher = defaultWatcher ?? new DefaultWatcher(this, are);
                    are.Reset();
                    zookeeper = new ZooKeeper(connectionString, SessionTimeout, defaultWatcher);
                    are.WaitOne(SessionTimeout);
                }
            }
            if (!Connected)
            {
                return false;
            }

            OnConnected?.Invoke();

            return true;
        }

        /// <summary>
        /// 关闭连接
        /// </summary>
        public void Close()
        {
            isClose = true;
            if (Connected)
            {
                zookeeper.closeAsync().Wait();
            }

        }
        /// <summary>
        /// 监控回调
        /// </summary>
        /// <param name="event">回调事件</param>
        /// <returns>异步</returns>
        public async override Task process(WatchedEvent @event)
        {
            ZookeeperEvent ze = new ZookeeperEvent(@event);

            if (!String.IsNullOrEmpty(ze.Path))
            {
                NodeWatcher watcher;
                if (nodeWatchers.TryGetValue(ze.Path, out watcher))
                {
                    if (watcher != null)
                    {
                        try
                        {
                            watcher.Process(ze);
                        }
                        catch { }
                        await WatchAsync(ze.Path, watcher, true);//重新监控
                    }
                }
            }
        }
        /// <summary>
        /// 修改当前目录地址
        /// </summary>
        /// <param name="path"></param>
        public void ChangeDirectory(String path)
        {
            this.CurrentPath = Combine(path);
        }

        /// <summary>
        /// 切换到相对目录下
        /// </summary>
        /// <param name="path"></param>
        public void Goto(String path)
        {
            this.CurrentPath = Combine(this.CurrentPath, path);
        }

        /// <summary>
        /// 使用认证
        /// </summary>
        /// <param name="scheme">认证类型</param>
        /// <param name="auth">认证数据</param>
        public void Authorize(AuthScheme scheme, String auth = "")
        {
            CheckConnection();
            zookeeper.addAuthInfo(scheme.ToString().ToLower(), Encoding.GetBytes(auth));
        }

        #region 监听与取消

        /// <summary>
        /// 对当前路径添加监控
        /// </summary>
        /// <param name="delegate">监控</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAsync(WatcherEvent @delegate)
        {
            return await WatchAsync(CurrentPath, @delegate, true);
        }

        /// <summary>
        /// 对当前路径添加监控
        /// </summary>
        /// <param name="watcher">监控</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAsync(NodeWatcher watcher)
        {
            return await WatchAsync(CurrentPath, watcher, true);
        }

        /// <summary>
        /// 对指定路径添加监控
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="delegate">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAsync(String path, WatcherEvent @delegate, Boolean isAbsolutePath = false)
        {
            var array = await WatchManyAsync(new String[] { path }, @delegate, isAbsolutePath);
            return array.FirstOrDefault();
        }

        /// <summary>
        /// 对指定路径添加监控
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="watcher">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAsync(String path, NodeWatcher watcher, Boolean isAbsolutePath = false)
        {
            var array = await WatchManyAsync(new String[] { path }, watcher, isAbsolutePath);
            return array.FirstOrDefault();
        }

        /// <summary>
        /// 监控多个路径，但是不包括子路径
        /// </summary>
        /// <param name="paths">节点路径</param>
        /// <param name="delegate">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean[]> WatchManyAsync(String[] paths, WatcherEvent @delegate, Boolean isAbsolutePath = false)
        {
            var watcher = new NodeWatcher();
            watcher.AllTypeChanged += @delegate;
            return await WatchManyAsync(paths, watcher, isAbsolutePath);
        }

        /// <summary>
        /// 监控多个路径，但是不包括子路径
        /// </summary>
        /// <param name="paths">节点路径</param>
        /// <param name="watcher">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean[]> WatchManyAsync(String[] paths, NodeWatcher watcher, Boolean isAbsolutePath = false)
        {
            CheckConnection();
            List<Boolean> list = new List<Boolean>();
            foreach (var path in paths)
            {
                try
                {
                    var p = GetAbsolutePath(path, isAbsolutePath);
                    if (await zookeeper.existsAsync(p, this) != null)
                    {
                        nodeWatchers[p] = watcher;
                        list.Add(true);
                    }
                    else
                    {
                        nodeWatchers.TryRemove(p, out _);
                        list.Add(false);
                    }
                }
                catch
                {
                    list.Add(false);
                }
            }
            return list.ToArray();
        }

        /// <summary>
        /// 监控当前路径，包括子路径
        /// </summary>
        /// <param name="delegate">监控</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAllAsync(WatcherEvent @delegate)
        {
            return await WatchAllAsync(CurrentPath, @delegate, true);
        }

        /// <summary>
        /// 监控当前路径，包括子路径
        /// </summary>
        /// <param name="watcher">监控</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAllAsync(NodeWatcher watcher)
        {
            return await WatchAllAsync(CurrentPath, watcher, true);
        }

        /// <summary>
        /// 监控指定路径，包括子路径
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="delegate">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAllAsync(String path, WatcherEvent @delegate, Boolean isAbsolutePath = false)
        {
            var array = await WatchAllAsync(new String[] { path }, @delegate, isAbsolutePath);
            return array.FirstOrDefault();
        }

        /// <summary>
        /// 监控指定路径，包括子路径
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="watcher">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean> WatchAllAsync(String path, NodeWatcher watcher, Boolean isAbsolutePath = false)
        {
            var array = await WatchAllAsync(new String[] { path }, watcher, isAbsolutePath);
            return array.FirstOrDefault();
        }

        /// <summary>
        /// 监控所有路径，包括子路径
        /// </summary>
        /// <param name="paths">节点路径</param>
        /// <param name="delegate">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean[]> WatchAllAsync(String[] paths, WatcherEvent @delegate, Boolean isAbsolutePath = false)
        {
            var watcher = new NodeWatcher();
            watcher.AllTypeChanged += @delegate;
            return await WatchAllAsync(paths, watcher, isAbsolutePath);
        }

        /// <summary>
        /// 监控所有路径，包括子路径
        /// </summary>
        /// <param name="paths">节点路径</param>
        /// <param name="watcher">监控</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步，true表示成功，false表示失败</returns>
        public async Task<Boolean[]> WatchAllAsync(String[] paths, NodeWatcher watcher, Boolean isAbsolutePath = false)
        {
            CheckConnection();
            List<Boolean> list = new List<Boolean>();
            foreach (var path in paths)
            {
                try
                {
                    var p = GetAbsolutePath(path, isAbsolutePath);
                    if (await zookeeper.existsAsync(p, this) != null)
                    {
                        nodeWatchers[p] = watcher;
                        list.Add(true);

                        var result = await zookeeper.getChildrenAsync(p);
                        await WatchAllAsync(result.Children.Select(c => Combine(p, c)).ToArray(), watcher, true);
                    }
                    else
                    {
                        nodeWatchers.TryRemove(p, out _);
                        list.Add(false);
                    }
                }
                catch
                {
                    list.Add(false);
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// 取消多个指定路径上的监控
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步</returns>
        public async Task CancelAsync(String path, Boolean isAbsolutePath = true)
        {
            await CancelAsync(new String[] { path }, isAbsolutePath);
        }

        /// <summary>
        /// 取消多个指定路径上的监控
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>异步</returns>
        public async Task CancelAsync(String[] paths, Boolean isAbsolutePath = true)
        {
            foreach (var path in paths)
            {
                var p = GetAbsolutePath(path, isAbsolutePath);
                nodeWatchers.TryRemove(p, out _);
                await Task.CompletedTask;
            }
        }

        /// <summary>
        /// 获取指定路径上的监控
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <param name="isAbsolutePath">是否绝对路径</param>
        /// <returns>存在则返回监控对象，否则返回null</returns>
        public NodeWatcher GetWatcher(String path, Boolean isAbsolutePath = true)
        {
            path = GetAbsolutePath(path, isAbsolutePath);
            NodeWatcher watcher;
            if (nodeWatchers.TryGetValue(path, out watcher))
            {
                return watcher;
            }

            return null;
        }
        #endregion

        #region 基本数据操作

        /// <summary>
        /// 当前节点是否存在
        /// </summary>
        /// <returns>存在返回true，否则返回false</returns>
        public Boolean Exists()
        {
            return ExistsAsync().GetAwaiter().GetResult();
        }

        /// <summary>
        /// 指定节点是否存在（相对当前节点）
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <returns>存在返回true，否则返回false</returns>
        public Boolean Exists(String path)
        {
            return ExistsAsync(path).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 指定节点是否存在
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <returns>存在返回true，否则返回false</returns>
        public Boolean ExistsByAbsolutePath(String absolutePath)
        {
            return ExistsByAbsolutePathAsync(absolutePath).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 当前节点是否存在
        /// </summary>
        /// <returns>异步，存在返回true，否则返回false</returns>
        public async Task<Boolean> ExistsAsync()
        {
            return await ExistsByAbsolutePathAsync(CurrentPath);
        }

        /// <summary>
        /// 指定节点是否存在（相对当前节点）
        /// </summary>
        /// <param name="path">节点路径</param>
        /// <returns>异步，存在返回true，否则返回false</returns>
        public async Task<Boolean> ExistsAsync(String path)
        {
            path = Combine(CurrentPath, path);
            return await ExistsByAbsolutePathAsync(path);
        }

        /// <summary>
        /// 指定节点是否存在
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <returns>异步，存在返回true，否则返回false</returns>
        public async Task<Boolean> ExistsByAbsolutePathAsync(String absolutePath)
        {
            absolutePath = Combine(absolutePath);
            return await zookeeper.existsAsync(absolutePath, false) != null;
        }

        /// <summary>
        /// 添加或者修改当前路径上的数据
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public String SetData(String value, Boolean persistent = false, Boolean sequential = false)
        {
            return SetDataAsync(value, persistent, sequential).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 添加或者修改指定相对路径上的数据
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public String SetData(String path, String value, Boolean persistent = false, Boolean sequential = false)
        {
            return SetDataAsync(path, value, persistent, sequential).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 添加或者修改指定绝对路径上的数据
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public String SetDataByAbsolutePath(String absolutePath, String value, Boolean persistent = false, Boolean sequential = false)
        {
            return SetDataByAbsolutePathAsync(absolutePath, value, persistent, sequential).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 添加或者修改当前路径上的数据
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public async Task<String> SetDataAsync(String value, Boolean persistent = false, Boolean sequential = false)
        {
            return await SetDataByAbsolutePathAsync(CurrentPath, value, persistent, sequential);
        }

        /// <summary>
        /// 添加或者修改指定相对路径上的数据
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public async Task<String> SetDataAsync(String path, String value, Boolean persistent = false, Boolean sequential = false)
        {
            path = Combine(CurrentPath, path);
            return await SetDataByAbsolutePathAsync(path, value, persistent, sequential);
        }

        /// <summary>
        /// 添加或者修改指定绝对路径上的数据
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <param name="value">数据</param>
        /// <param name="persistent">是否持久节点</param>
        /// <param name="sequential">是否顺序节点</param>
        /// <returns>znode节点名，不包含父节点路径</returns>
        public async Task<String> SetDataByAbsolutePathAsync(String absolutePath, String value, Boolean persistent = false, Boolean sequential = false)
        {
            CheckConnection();
            CheckWriten();

            absolutePath = Combine(absolutePath);
            if (null == await zookeeper.existsAsync(absolutePath, false))
            {
                absolutePath = await zookeeper.createAsync(absolutePath, Encoding.GetBytes(value), defaultACL, persistent ?
                    sequential ? CreateMode.PERSISTENT_SEQUENTIAL : CreateMode.PERSISTENT :
                    sequential ? CreateMode.EPHEMERAL_SEQUENTIAL : CreateMode.EPHEMERAL);
            }
            else
            {
                await zookeeper.setDataAsync(absolutePath, Encoding.GetBytes(value));
            }
            return absolutePath.Split(new String[] { sep }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
        }

        /// <summary>
        /// 获取指定相对路径上的数据
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <returns>相对路径上的数据</returns>
        public String GetData(String path)
        {
            return GetDataAsync(path).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 获取指定绝对路径上的数据
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <returns>相对路径上的数据</returns>
        public String GetDataByAbsolutePath(String absolutePath)
        {
            return GetDataByAbsolutePathAsync(absolutePath).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 获取指定相对路径上的数据
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <returns>相对路径上的数据</returns>
        public async Task<String> GetDataAsync(String path)
        {
            path = Combine(CurrentPath, path);
            return await GetDataByAbsolutePathAsync(path);
        }

        /// <summary>
        /// 获取指定绝对路径上的数据
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <returns>绝对路径上的数据</returns>
        public async Task<String> GetDataByAbsolutePathAsync(String absolutePath)
        {
            CheckConnection();
            absolutePath = Combine(absolutePath);
            if (await zookeeper.existsAsync(absolutePath, false) == null)
            {
                return "";
            }
            var data = await zookeeper.getDataAsync(absolutePath, false);
            return Encoding.GetString(data.Data);
        }

        /// <summary>
        /// 获取指定节点及其字节点的所有值，使用路径做键返回字典型
        /// </summary>
        /// <param name="sep"></param>
        /// <returns></returns>
        public async Task<IDictionary<String, String>> GetDictionaryAsync(String sep = ":")
        {
            CheckConnection();
            Dictionary<String, String> dict = new Dictionary<String, String>();
            async Task action(String path)
            {
                var result = await zookeeper.getChildrenAsync(path, false);
                String name = MakePathName(sep, path);
                dict[name] = await GetDataByAbsolutePathAsync(path);
                foreach (var child in result.Children)
                {
                    var p = Combine(path, child);
                    await action(p);
                }
            }

            await action(CurrentPath);
            return dict;
        }

        /// <summary>
        /// 获取子节点
        /// </summary>
        /// <param name="path">相对路径</param>
        /// <param name="order">是否按时间排序</param>
        /// <returns>子节点数组（节点路径不含父节点路径）</returns>
        public async Task<String[]> GetChildrenAsync(String path, Boolean order = false)
        {
            path = Combine(CurrentPath, path);
            return await GetChildrenByAbsolutePathAsync(path, order);
        }

        /// <summary>
        /// 获取指定路径绝对路径下的子节点
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        /// <param name="order">是否按时间排序</param>
        /// <returns>子节点数组（节点路径不含父节点路径）</returns>
        public async Task<String[]> GetChildrenByAbsolutePathAsync(String absolutePath, Boolean order = false)
        {
            var result = await zookeeper.getChildrenAsync(absolutePath, false);
            if (!order)
            {
                return result.Children.ToArray();
            }

            List<(String, long)> list = new List<(String, long)>();
            foreach (var child in result.Children)
            {
                var p = Combine(absolutePath, child);
                var stat = await zookeeper.existsAsync(p, false);
                if (stat != null)
                {
                    list.Add((child, stat.getCtime()));
                }
            }

            return list.OrderBy(l => l.Item2).Select(l => l.Item1).ToArray();
        }

        /// <summary>
        /// 移除当前路径节点
        /// </summary>
        public void Delete()
        {
            DeleteAsync().Wait();
        }

        /// <summary>
        /// 移除相对当前的指定路径节点及子节点
        /// </summary>
        /// <param name="path">相对路径</param>
        public void Delete(String path)
        {
            DeleteAsync(path).Wait();
        }

        /// <summary>
        /// 移除指定绝对路径节点及子节点
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        public void DeleteByAbsolutePath(String absolutePath)
        {
            DeleteByAbsolutePathAsync(absolutePath).Wait();
        }

        /// <summary>
        /// 移除当前路径节点
        /// </summary>
        public async Task DeleteAsync()
        {
            await DeleteByAbsolutePathAsync(CurrentPath);
        }

        /// <summary>
        /// 移除相对当前的指定路径节点及子节点
        /// </summary>
        /// <param name="path">相对路径</param>
        public async Task DeleteAsync(String path)
        {
            path = Combine(CurrentPath, path);
            await DeleteByAbsolutePathAsync(path);
        }

        /// <summary>
        /// 移除指定绝对路径节点及子节点
        /// </summary>
        /// <param name="absolutePath">绝对路径</param>
        public async Task DeleteByAbsolutePathAsync(String absolutePath)
        {
            if (await ExistsByAbsolutePathAsync(absolutePath))
            {
                var children = await GetChildrenByAbsolutePathAsync(absolutePath);
                foreach (var child in children)
                {
                    var path = Combine(absolutePath, child);
                    await DeleteByAbsolutePathAsync(path);
                }

                absolutePath = Combine(absolutePath);
                await zookeeper.deleteAsync(absolutePath);
            }
        }
        #endregion

        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            OnDisposing?.Invoke();
            Close();
            timer?.Dispose();
            nodeWatchers?.Clear();
            are?.Dispose();
            GC.Collect();
        }

        public class InitWatcher : Watcher
        {
            public String Name { get; private set; }
            public InitWatcher(String name)
            {
                Name = name;
            }
            public override Task process(WatchedEvent @event)
            {
                var path = @event.getPath();
                var state = @event.getState();

                 Console.WriteLine($"{Name} recieve: Path-{path}     State-{state}    Type-{@event.get_Type()}");
                return Task.FromResult(0);
            }
        }


        /// <summary>
        /// 默认的监听器，用于初始化使用
        /// </summary>
        public class DefaultWatcher : Watcher
        {
            /// <summary>
            /// waithandle同步
            /// </summary>
            EventWaitHandle ewh;
            /// <summary>
            /// 辅助类
            /// </summary>
            ZookeeperHelper zookeeperHelper;

            public DefaultWatcher(ZookeeperHelper zookeeperHelper, EventWaitHandle ewh)
            {
                this.ewh = ewh;
                this.zookeeperHelper = zookeeperHelper;
            }
            /// <summary>
            /// 回调
            /// </summary>
            /// <param name="event">监听事件对象</param>
            /// <returns></returns>
            public override Task process(WatchedEvent @event)
            {
                var state = @event.getState();
                if (state == Event.KeeperState.ConnectedReadOnly || state == Event.KeeperState.SyncConnected)//连接时
                {
                    ewh.Set();
                }
                //回话过期重新建立连接
                else if ((state == Event.KeeperState.Expired) && !zookeeperHelper.isClose)
                {
                    zookeeperHelper.timer.Enabled = true;
                }


                return Task.FromResult(1);
            }
        }
    }

    /// <summary>
    /// 认证类型
    /// </summary>
    public enum AuthScheme
    {
        /// <summary>
        /// 下面只有一个id，叫anyone，world:anyone代表任何人，zookeeper中对所有人有权限的结点就是属于world:anyone类型的。创建节点的默认权限。有唯一的id是anyone授权的时候的模式为 world:anyone:rwadc 表示所有人都对这个节点有rwadc的权限
        /// </summary>
        World = 0,
        /// <summary>
        ///不需要id,只要是通过authentication的user都有权限（zookeeper支持通过kerberos来进行authencation, 也支持username/password形式的authentication)
        /// </summary>
        Auth = 1,
        /// <summary>
        /// 它对应的id为username:BASE64(SHA1(password))，它需要先通过加密过的username:password形式的authentication。
        /// </summary>
        Digest = 2,
        /// <summary>
        ///它对应的id为客户机的IP地址，设置的时候可以设置一个ip段，比如ip:192.168.1.0/16。
        /// </summary>
        Ip = 3,
        /// <summary>
        /// 在这种scheme情况下，对应的id拥有超级权限，可以做任何事情(cdrwa）
        /// </summary>
        Super = 4
    }

    /// <summary>
    /// Zookeeper事件数据
    /// </summary>
    public class ZookeeperEvent
    {
        public ZookeeperEvent(WatchedEvent @event)
        {
            switch (@event.getState())
            {
                case Watcher.Event.KeeperState.AuthFailed: State = EventState.AuthFailed; break;
                case Watcher.Event.KeeperState.ConnectedReadOnly: State = EventState.ConnectedReadOnly; break;
                case Watcher.Event.KeeperState.Disconnected: State = EventState.Disconnected; break;
                case Watcher.Event.KeeperState.Expired: State = EventState.Expired; break;
                case Watcher.Event.KeeperState.SyncConnected: State = EventState.SyncConnected; break;
            }

            switch (@event.get_Type())
            {
                case Watcher.Event.EventType.NodeChildrenChanged: Type = EventType.NodeChildrenChanged; break;
                case Watcher.Event.EventType.NodeCreated: Type = EventType.NodeCreated; break;
                case Watcher.Event.EventType.NodeDataChanged: Type = EventType.NodeDataChanged; break;
                case Watcher.Event.EventType.NodeDeleted: Type = EventType.NodeDeleted; break;
                case Watcher.Event.EventType.None: Type = EventType.None; break;
            }

            Path = @event.getPath();
        }
        /// <summary>
        /// 当前连接状态
        /// </summary>
        public EventState State { get; private set; }
        /// <summary>
        /// 事件类型
        /// </summary>
        public EventType Type { get; private set; }
        /// <summary>
        /// 事件路径
        /// </summary>
        public String Path { get; private set; }

        /// <summary>
        /// 连接状态
        /// </summary>
        public enum EventState
        {
            /// <summary>
            /// 超时
            /// </summary>
            Expired = -112,
            /// <summary>
            /// 连接已断开
            /// </summary>
            Disconnected = 0,
            /// <summary>
            /// 已建立连接
            /// </summary>
            SyncConnected = 3,
            /// <summary>
            /// 认证失败
            /// </summary>
            AuthFailed = 4,
            /// <summary>
            /// 已建立连接，但是只支持只读模式
            /// </summary>
            ConnectedReadOnly = 5
        }
        /// <summary>
        /// 时间类型
        /// </summary>
        public enum EventType
        {
            /// <summary>
            /// 空类型，如：建立连接时
            /// </summary>
            None = -1,
            /// <summary>
            /// 节点创建时
            /// </summary>
            NodeCreated = 1,
            /// <summary>
            /// 节点删除时
            /// </summary>
            NodeDeleted = 2,
            /// <summary>
            /// 节点数据改变时
            /// </summary>
            NodeDataChanged = 3,
            /// <summary>
            /// 节点增加子节点时
            /// </summary>
            NodeChildrenChanged = 4
        }
    }
    /// <summary>
    /// 监控对象
    /// </summary>
    public class NodeWatcher
    {
        /// <summary>
        /// 节点创建时调用事件
        /// </summary>
        public event WatcherEvent NodeCreated;
        /// <summary>
        /// 节点删除时调用事件
        /// </summary>
        public event WatcherEvent NodeDeleted;
        /// <summary>
        /// 节点数据改变时调用事件
        /// </summary>
        public event WatcherEvent NodeDataChanged;
        /// <summary>
        /// 节点增加子节点时调用事件
        /// </summary>
        public event WatcherEvent NodeChildrenChanged;
        /// <summary>
        /// 不区分类型，所有的类型都会调用
        /// </summary>
        public event WatcherEvent AllTypeChanged;

        /// <summary>
        /// 触发，执行事件
        /// </summary>
        /// <param name="event"></param>
        public void Process(ZookeeperEvent @event)
        {
            try
            {
                switch (@event.Type)
                {
                    case ZookeeperEvent.EventType.NodeChildrenChanged:
                        NodeChildrenChanged?.Invoke(@event);
                        break;
                    case ZookeeperEvent.EventType.NodeCreated:
                        NodeCreated?.Invoke(@event);
                        break;
                    case ZookeeperEvent.EventType.NodeDeleted:
                        NodeDeleted?.Invoke(@event);
                        break;
                    case ZookeeperEvent.EventType.NodeDataChanged:
                        NodeDataChanged?.Invoke(@event);
                        break;
                }

                AllTypeChanged?.Invoke(@event);
            }
            catch { }
        }
    }

    /// <summary>
    /// 监控事件委托
    /// </summary>
    /// <param name="event"></param>
    public delegate void WatcherEvent(ZookeeperEvent @event);

    /// <summary>
    /// Zookeeper默认日志记录
    /// </summary>
    public class ZookeeperLogger : ILogConsumer
    {
        /// <summary>
        /// 是否记录日志到文件
        /// </summary>
        public Boolean LogToFile { get; set; } = false;
        /// <summary>
        /// 是否记录堆栈信息
        /// </summary>
        public Boolean LogToTrace { get; set; } = true;
        /// <summary>
        /// 日志级别
        /// </summary>
        public TraceLevel LogLevel { get; set; } = TraceLevel.Warning;

        /// <summary>
        /// 日志记录
        /// </summary>
        /// <param name="severity"></param>
        /// <param name="className"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public virtual void Log(TraceLevel severity, String className, String message, Exception exception)
        {
            Console.WriteLine(String.Format("Level:{0}  className:{1}   message:{2}", severity, className, message));
            Console.WriteLine(exception.StackTrace);
        }
    }
}
