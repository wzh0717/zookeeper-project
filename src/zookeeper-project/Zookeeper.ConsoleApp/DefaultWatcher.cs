﻿using org.apache.zookeeper;
using System;
using System.Threading.Tasks;

namespace Zookeeper.ConsoleApp
{
    /// <summary>
    /// 默认回调监听器
    /// created by wangzh 2020/06/12
    /// </summary>
    public class DefaultWatcher : Watcher
    {
        public String Name { get; private set; }
        public DefaultWatcher(String name)
        {
            Name = name;
        }
        public override Task process(WatchedEvent @event)
        {
            var path = @event.getPath();
            var state = @event.getState();

            Console.WriteLine($"{Name} recieve: Path-{path}     State-{state}    Type-{@event.get_Type()}");
            return Task.FromResult(0);
        }
    }
}
