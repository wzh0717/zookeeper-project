﻿using org.apache.zookeeper;
using org.apache.zookeeper.data;
using System;
using System.Collections.Generic;
using System.Threading;
using Zookeeper.Client;

namespace Zookeeper.ConsoleApp
{
    class Program
    {
        const Int32 sessionTimeout = 10000;
        const String connectionString = "192.168.20.245:2181";
        static List<ACL> acl = ZooDefs.Ids.OPEN_ACL_UNSAFE;
        const String path = "/mynode3456";
        const String data = "Hello World !";
        //static void Main(string[] args)
        //{
        //    var zk = new ZooKeeper(connectionString, sessionTimeout, new DefaultWatcher("Connection"), false);
            
        //    while (ZooKeeper.States.CONNECTING.Equals(zk.getState()))
        //    {
        //        Console.WriteLine($"{connectionString} 连接中...");
        //        Thread.Sleep(1000);
        //    }

        //    Console.WriteLine($"连接状态：{zk.getState()}");
        //    //模拟等待连接
        //    Thread.Sleep(1000);

        //    if (zk.getState() == ZooKeeper.States.CONNECTED ||
        //        zk.getState() == ZooKeeper.States.CONNECTEDREADONLY)
        //        Console.WriteLine("连接成功...");

        //    //节点是否存在
        //    var exists = zk.existsAsync(path, new DefaultWatcher("Exists")).GetAwaiter().GetResult();
        //    Console.WriteLine($"节点是否存在：{exists}");

        //    //创建节点
        //    var create = zk.createAsync(path, Encoding.UTF8.GetBytes(data), acl, CreateMode.PERSISTENT).GetAwaiter().GetResult();
        //    Console.WriteLine($"创建节点：{create}");

        //    //获取节点数据
        //    var result1 = zk.getDataAsync(path, new DefaultWatcher("Get")).GetAwaiter().GetResult();
        //    Console.WriteLine("完成读取节点：" + Encoding.UTF8.GetString(result1.Data));


        //    //设置节点数据
        //    var set = zk.setDataAsync(path, Encoding.UTF8.GetBytes("我们家三代都是中国人")).GetAwaiter().GetResult();
        //    Console.WriteLine($"重新设置节点数据：{set}");


        //    //重新获取节点数据
        //    var result2 = zk.getDataAsync(path, new DefaultWatcher("GetAgain")).GetAwaiter().GetResult();
        //    Console.WriteLine("重新获取节点数据：" + Encoding.UTF8.GetString(result2.Data));


        //    //移除节点
        //    zk.deleteAsync(path).GetAwaiter().GetResult();
        //    Console.WriteLine("移除节点");

        //    Console.ReadLine();
        //}


        static void Main(string[] args)
        {
            ZookeeperHelper zookeeperHelper = new ZookeeperHelper(connectionString, "/");
            zookeeperHelper.SessionTimeout = sessionTimeout;
            zookeeperHelper.Connect();//发起连接

            while (!zookeeperHelper.Connected)
            {
                Console.WriteLine("连接中...");
                Thread.Sleep(1000); //模拟连接等待时间
            }

            //创建znode节点
            {
                zookeeperHelper.SetData(path, data, true, false);
                Console.WriteLine("完成创建节点");
            }

            //节点是否存在
            {
                var exists = zookeeperHelper.Exists(path);
                Console.WriteLine("节点是否存在：" + exists);
            }

            //添加监听器
            {
                zookeeperHelper.WatchAsync(path, (e) =>
                {
                    Console.WriteLine($"recieve: Path-{e.Path}     State-{e.State}    Type-{e.Type}");
                }).GetAwaiter().GetResult();
            }

            //获取节点数据
            {
                var value = zookeeperHelper.GetData(path);
                Console.WriteLine("完成读取节点：" + value);
            }

            //设置节点数据
            {
                zookeeperHelper.SetData(path, "hello world again");
                Console.WriteLine("设置节点数据");
            }

            //重新获取节点数据
            {
                var value = zookeeperHelper.GetData(path);
                Console.WriteLine("重新获取节点数据：" + value);
            }

            //移除节点
            {
                zookeeperHelper.Delete(path);
                Console.WriteLine("移除节点");
            }

            Console.WriteLine("完成");
            Console.ReadLine();

        }
    }
}
